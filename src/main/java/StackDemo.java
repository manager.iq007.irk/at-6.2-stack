public class StackDemo {
    public static void main(String[] args) {
        MyStack<String> stack = new MyStack<>();

        stack.push("футболка");
        stack.push("кардиган");
        stack.push("носки");
        stack.push("джинсы");
        stack.push("брюки");

        for (int i = 0; i < 5; i++) {
            String currentString = (String) stack.pop();
            System.out.println(currentString);
        }

        MyStack<String> stackBugs = new MyStack<>();

        for (int i = 0; i < 5; i++) {
            String currentString = (String) stackBugs.pop();
            System.out.println(currentString);
        }

        stackBugs.push("нижняя булочка");
        stackBugs.push("соус");
        stackBugs.push("салат");
        stackBugs.push("котлета");
        stackBugs.push("помидор");
        stackBugs.push("верхняя булочка");

    }
}
