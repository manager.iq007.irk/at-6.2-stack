public class MyStack<T> {

    T[] elements = (T[]) new Object[5];
    int index = -1;

    /**
     * Дженерик - это сущность, которая хранит в себе разные данные одного из ссылочных типов, или же способ описать эти данные.
     * <p>
     * Параметр T в коде класса означает именно тот тип данных, с которым работает дженерик (например, String, Integer)
     * <p>
     * или попытка получить элемент из пустого стека - не приводили к ошибке.
     * <p>
     * Опционально - подумайте, как можно сократить методы pop и push.
     */


    //класть (с обработкой ошибки)
    public void push(T element) {
        try {
            index++;
            elements[index] = element;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Стэк заполнен, добавить новый элемент невозможно! Максимальная вместимость стека - " + elements.length);
        }
    }

    //брать
    public T pop() {
       try {
            T element = elements[index];
            elements[index] = null;
            index--;
            return element;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Стэк пустой, невозможно получить элемент!");
      }
    }
}


